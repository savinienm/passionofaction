<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221125132520 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE composition_item (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE perfume_composition_item (perfume_id INT NOT NULL, composition_item_id INT NOT NULL, INDEX IDX_C1665A63AA91F2AA (perfume_id), INDEX IDX_C1665A63F184526 (composition_item_id), PRIMARY KEY(perfume_id, composition_item_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE perfume_composition_item ADD CONSTRAINT FK_C1665A63AA91F2AA FOREIGN KEY (perfume_id) REFERENCES perfume (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE perfume_composition_item ADD CONSTRAINT FK_C1665A63F184526 FOREIGN KEY (composition_item_id) REFERENCES composition_item (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE perfume DROP composition');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE perfume_composition_item DROP FOREIGN KEY FK_C1665A63AA91F2AA');
        $this->addSql('ALTER TABLE perfume_composition_item DROP FOREIGN KEY FK_C1665A63F184526');
        $this->addSql('DROP TABLE composition_item');
        $this->addSql('DROP TABLE perfume_composition_item');
        $this->addSql('ALTER TABLE perfume ADD composition LONGTEXT NOT NULL');
    }
}
