<?php

namespace App\Controller;

use App\Entity\CompositionItem;
use App\Form\CompositionItemType;
use App\Repository\CompositionItemRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/composition/item')]
class CompositionItemController extends AbstractController
{
    #[Route('/', name: 'app_composition_item_index', methods: ['GET'])]
    public function index(CompositionItemRepository $compositionItemRepository): Response
    {
        return $this->render('composition_item/index.html.twig', [
            'items' => $compositionItemRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_composition_item_new', methods: ['GET', 'POST'])]
    public function new(Request $request, CompositionItemRepository $compositionItemRepository): Response
    {
        $compositionItem = new CompositionItem();
        $form = $this->createForm(CompositionItemType::class, $compositionItem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $compositionItemRepository->save($compositionItem, true);

            return $this->redirectToRoute('app_composition_item_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('composition_item/new.html.twig', [
            'composition_item' => $compositionItem,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_composition_item_show', methods: ['GET'])]
    public function show(CompositionItem $compositionItem): Response
    {
        return $this->render('composition_item/show.html.twig', [
            'composition_item' => $compositionItem,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_composition_item_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, CompositionItem $compositionItem, CompositionItemRepository $compositionItemRepository): Response
    {
        $form = $this->createForm(CompositionItemType::class, $compositionItem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $compositionItemRepository->save($compositionItem, true);

            return $this->redirectToRoute('app_composition_item_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('composition_item/edit.html.twig', [
            'composition_item' => $compositionItem,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_composition_item_delete', methods: ['POST'])]
    public function delete(Request $request, CompositionItem $compositionItem, CompositionItemRepository $compositionItemRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$compositionItem->getId(), $request->request->get('_token'))) {
            $compositionItemRepository->remove($compositionItem, true);
        }

        return $this->redirectToRoute('app_composition_item_index', [], Response::HTTP_SEE_OTHER);
    }
}
