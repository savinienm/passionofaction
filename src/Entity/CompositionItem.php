<?php

namespace App\Entity;

use App\Repository\CompositionItemRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CompositionItemRepository::class)]
class CompositionItem
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\ManyToMany(targetEntity: Perfume::class, mappedBy: 'composition')]
    private Collection $perfumes;

    public function __construct()
    {
        $this->perfumes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Perfume>
     */
    public function getPerfumes(): Collection
    {
        return $this->perfumes;
    }

    public function addPerfume(Perfume $perfume): self
    {
        if (!$this->perfumes->contains($perfume)) {
            $this->perfumes->add($perfume);
            $perfume->addComposition($this);
        }

        return $this;
    }

    public function removePerfume(Perfume $perfume): self
    {
        if ($this->perfumes->removeElement($perfume)) {
            $perfume->removeComposition($this);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }
}
