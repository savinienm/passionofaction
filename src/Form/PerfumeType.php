<?php

namespace App\Form;

use App\Entity\Perfume;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\CompositionItem;

class PerfumeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('price')
            ->add('image')
            ->add('composition', EntityType::class, [
                'class' => CompositionItem::class,
                'label' =>false,
                // Permet la disposition en checkbox
                'expanded' => true,
                // Permet les choix multiples
                'multiple' => true
            ]
)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Perfume::class,
        ]);
    }
}
